package some.pack;

import org.mapstruct.factory.Mappers;
import some.pack.dto.Source;
import some.pack.dto.Target;
import some.pack.mapper.ImmutableMapper;

public class Main {
    public static void main(String[] args) {
        ImmutableMapper mapper = Mappers.getMapper(ImmutableMapper.class);
        Source source = Source.builder().name("some name").list("list").build();
        Target target;
        target = mapper.map(source);
        System.out.println(target);
        source = mapper.reverseMap(target);
        System.out.println(source);
    }
}
