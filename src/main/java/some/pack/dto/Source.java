package some.pack.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Source {
    private String name;
    private String list;
}
