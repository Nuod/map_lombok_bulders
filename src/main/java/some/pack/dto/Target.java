package some.pack.dto;

import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Target {
    private String name;
    private String list;

}
