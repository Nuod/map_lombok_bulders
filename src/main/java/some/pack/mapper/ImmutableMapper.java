package some.pack.mapper;

import org.mapstruct.Mapper;
import some.pack.dto.Source;
import some.pack.dto.Target;

@Mapper
public interface ImmutableMapper {

    Target map(Source source);

    Source reverseMap(Target target);
}
